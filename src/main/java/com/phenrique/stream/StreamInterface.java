package com.phenrique.stream;

/**
 * Interface com métodos utilizados para percorrer uma stream
 * 
 * @author paulo.henrique
 *
 */
public interface StreamInterface {

	/**
	 * Método que obtém o próximo elemento dentro da stream fornecida pelo usuário
	 * @return boolean
	 */
	public char getNext();

	/**
	 * Método que verifica se existe ou não um próximo elemento dentro da stream
	 * fornecida pelo usuário
	 * 
	 * @return boolean
	 */
	public boolean hasNext();

}
