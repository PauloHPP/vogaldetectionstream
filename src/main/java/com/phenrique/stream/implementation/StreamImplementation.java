package com.phenrique.stream.implementation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.NoSuchElementException;

import com.phenrique.stream.StreamInterface;

/**
 * Classe que implementa o critério: Encontrar dentro de uma stream a primeira
 * vogal que não seja repetida e que seja sucessora de uma consoante que por sua
 * vez é sucessora de uma vogal
 * 
 * @author paulo.henrique
 *
 */
public class StreamImplementation {

	/**
	 * Construtor privado, para evitar que a classe seja instanciada, visto que
	 * só existe um método estático exposto (e o método main)
	 */
	private StreamImplementation() {
	}

	/**
	 * Método que valida se a sequencia de caracteres se encaixa nos critérios
	 * estabelecidos e retorna a vogal desejada
	 * 
	 * @param input
	 * @return firstChar
	 */
	private static char firstChar(String input) {

		StreamInterface firstCharResult = getFirstChar(input);
		char old;
		char middle = 0;
		char actual = firstCharResult.getNext();

		do {
			old = middle;
			middle = actual;
			actual = firstCharResult.getNext();
			if (isVogal(old) && !isVogal(middle) && (isVogal(actual) && !isVogalRepetida(actual, input))) {
				return actual;
			}
		} while (firstCharResult.hasNext());
		return 0;
	}

	/**
	 * Método que verifica se a vogal aparece mais de uma vez dentro de uma
	 * stream
	 * 
	 * @param letra
	 * @param texto
	 * @return boolean
	 */
	private static boolean isVogalRepetida(char letra, String texto) {
		int count = texto.length() - texto.replace(String.valueOf(letra), "").length();
		if (count > 1) {
			return true;
		}
		return false;
	}

	/**
	 * Método que verifica se o caracter passado é ou não uma vogal
	 * 
	 * @param letra
	 * @return boolean
	 */
	private static boolean isVogal(char letra) {
		char[] vogais = { 'a', 'e', 'i', 'o', 'u' };
		for (char v : vogais) {
			if (String.valueOf(letra).equalsIgnoreCase(String.valueOf(v))) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Método que implementa a interface StreamInterface, para percorrer a
	 * stream passada
	 * 
	 * @param s
	 * @return StreamInterface
	 */
	private static StreamInterface getFirstChar(final String s) {
		return new StreamInterface() {
			int index = 0;

			public boolean hasNext() {
				return index < s.length();
			}

			public char getNext() {
				if (hasNext()) {
					return s.charAt(index++);
				} else {
					throw new NoSuchElementException();
				}
			}
		};
	}

	/**
	 * Método exposto, que retorna a vogal encontrada ou mostra uma mensagem que
	 * avisa que nenhuma vogal que atenda ao critérios foi encontrada
	 * 
	 * @param text
	 * @return
	 */
	public static String returnFirstValidVogal(String text) {
		char firstChar = firstChar(text);
		if (firstChar != 0) {
			return String.valueOf(firstChar);
		} else {
			return "Não existe vogal que se encaixa nos critérios descritos";
		}
	}

	/**
	 * Método main, executável, utilizado para solicitar ao usuário a digitação
	 * da stream para verificação dos caracteres e detecção de vogal que atenda
	 * aos critérios estabelecidos
	 * 
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		System.out.println("A aplicacao encontrará a primeira vogal que não seja repetida e "
				+ "que seja sucessora de uma consoante que por sua vez é sucessora de uma vogal");
		System.out.print("Digite uma sequência de letras: ");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String print = br.readLine();

		String vogal = returnFirstValidVogal(print);
		if (vogal.length() > 1) {
			System.out.println(vogal);
		} else {
			System.out.println("A vogal que atende aos critérios é: " + vogal);

		}
	}
}
