package com.phenrique.stream.implementation;

import static org.junit.Assert.*;

import org.junit.Test;

import com.phenrique.stream.implementation.StreamImplementation;

public class StreamImplementationTest {

	@Test
	public void testRetunVogalI(){
		String result = StreamImplementation.returnFirstValidVogal("teasdsdSasdfebaffgsewreobegoddoudgbufIxt");
		assertEquals("I", result);
	}
	
	@Test
	public void testVogalNotValid() {
		String result = StreamImplementation.returnFirstValidVogal("teasdsdSasdfebaffgsewreobegoddoudgbufIxtI");
		assertEquals("Não existe vogal que se encaixa nos critérios descritos", result);
	}
	
	@Test
	public void testRetunVogale(){
		String result = StreamImplementation.returnFirstValidVogal("aAbBABacafe");
		assertEquals("e", result);
	}
	
	@Test
	public void testRetunVogalA(){
		String result = StreamImplementation.returnFirstValidVogal("erteewIOUOniKgIuiyUnneqa");
		assertEquals("a", result);
	}
	
	@Test
	public void testRetunVogalU(){
		String result = StreamImplementation.returnFirstValidVogal("erteewIOOniKgIiyUnneq");
		assertEquals("U", result);
	}
}
