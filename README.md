# Leitor de Stream #

Pequena implementação que detecta uma vogal que não se repete em uma sequencia de caracteres, que seja sucessora de uma consoante que por sua vez sucede outra vogal.

- É utilizada uma interface que define os métodos `getNext()` e `hasNext()` que precisam ser implementados